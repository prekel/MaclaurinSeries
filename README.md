# MaclaurinSeries

[![Build Status](https://gitlab.com/prekel/MaclaurinSeries/badges/master/pipeline.svg)](https://gitlab.com/prekel/MaclaurinSeries/pipelines) 

Вычисляет [некоторые функции](https://gitlab.com/prekel/MaclaurinSeries/wikis/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D1%84%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D0%B9), используя ряды Маклорена.

![Screenshot](Screenshot.png)

## Использование в коде
```c++
SinX f;

f.setX(0);
f.setEps(1e-8);
f.Calculate();

auto res = f.getResult();

EXPECT_LT((0 - res), 1e-8);
```