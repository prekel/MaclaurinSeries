QT       -= core gui

TARGET = MaclaurinSeriesLib
TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    AbstractFunction.cpp \
    Functions/SinX.cpp \
    AbstractFunctionX.cpp \
    AbstractFunctionXY.cpp \
    Functions/ExpX.cpp \
    Functions/CosX.cpp \
    Functions/LnOnePlusX.cpp \
    Functions/PowOnePlusXY.cpp \
    Functions/AtanX.cpp \
    Functions/SumXY.cpp \
    Functions/TanX.cpp \
    Functions/SecX.cpp

HEADERS += \
    AbstractFunction.h \
    Functions/SinX.h \
    AbstractFunctionX.h \
    AbstractFunctionXY.h \
    Functions/ExpX.h \
    Functions/CosX.h \
    Functions/LnOnePlusX.h \
    Functions/PowOnePlusXY.h \
    Functions/AtanX.h \
    Functions/All.h \
    ReturnCode.h \
    Functions/SumXY.h \
    Functions/TanX.h \
    Functions/SecX.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
