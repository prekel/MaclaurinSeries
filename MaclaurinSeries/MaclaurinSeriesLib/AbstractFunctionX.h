#ifndef ABSTRACTFUNCTIONX_H
#define ABSTRACTFUNCTIONX_H

#include <AbstractFunction.h>

///
/// \brief Абстрактный класс для функций с одним аргументом.
///
class AbstractFunctionX : public AbstractFunction
{
public:
	AbstractFunctionX() = default;
	virtual ~AbstractFunctionX() override = default;

	///
	/// \brief Возвращает значение аргумента.
	/// \return Значение аргумента
	///
	double getX()
	{
		return m_x;
	}

	///
	/// \brief Устанавливает значение аргумента.
	/// \param x Значение аргумента
	///
	void setX(double x)
	{
		m_isCalculated = false;
		m_x = x;
	}

	std::string ToString() override;
	std::string ToStringArgs() override;
	std::string ToStringArgsResult() override;

protected:
	///
	/// \brief Аргумент X.
	///
	double m_x;
};

#endif // ABSTRACTFUNCTIONX_H
