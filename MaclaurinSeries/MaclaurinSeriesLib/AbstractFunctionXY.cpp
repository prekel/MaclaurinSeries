#include "AbstractFunctionXY.h"

std::string AbstractFunctionXY::ToString()
{
	return getName() + "(x, y)";
}

std::string AbstractFunctionXY::ToStringArgs()
{
	return getName() + "(" + std::to_string(getX()) + ", " + std::to_string(getY()) + ")";
}

std::string AbstractFunctionXY::ToStringArgsResult()
{
	return getName() + "(" + std::to_string(getX()) + ", " + std::to_string(getY()) + ") = " + std::to_string(getResult());
}
