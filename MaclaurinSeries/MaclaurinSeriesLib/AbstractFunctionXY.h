#ifndef ABSTRACTFUNCTIONXY_H
#define ABSTRACTFUNCTIONXY_H

#include <AbstractFunction.h>

///
/// \brief Абстрактный класс для функций с двумя аргументами.
///
class AbstractFunctionXY : public AbstractFunction
{
public:
	AbstractFunctionXY() = default;
	virtual ~AbstractFunctionXY() override = default;

	///
	/// \brief Возвращает значение первого аргумента.
	/// \return Значение первого аргумента
	///
	double getX()
	{
		return m_x;
	}

	///
	/// \brief Устанавливает значение первого аргумента.
	/// \param x Значение первого аргумента
	///
	void setX(double x)
	{
		m_isCalculated = false;
		m_x = x;
	}

	///
	/// \brief Возвращает значение второго аргумента.
	/// \return Значение второго аргумента
	///
	double getY()
	{
		return m_y;
	}

	///
	/// \brief Устанавливает значение второго аргумента.
	/// \param x Значение второго аргумента
	///
	void setY(double y)
	{
		m_isCalculated = false;
		m_y = y;
	}

	std::string ToString() override;
	std::string ToStringArgs() override;
	std::string ToStringArgsResult() override;

protected:
	///
	/// \brief Аргумент X.
	///
	double m_x;

	///
	/// \brief Аргумент Y.
	///
	double m_y;
};

#endif // ABSTRACTFUNCTIONXY_H
