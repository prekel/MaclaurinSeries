//#ifndef RETURNCODE_H
//#define RETURNCODE_H

// ///
// /// \brief Перечисление, представляющее статус вычисления.
// ///
//enum class ReturnCode
//{
//	///
//	/// \brief Успешно.
//	///
//	Success = 0,
//	///
//	/// \brief Неудача, неизвестная причина.
//	///
//	Fail = -1,
//	///
//	/// \brief Неудача, значения аргумента не в зоне определения.
//	///
//	NotInDefinitionArea = -2,
//	///
//	/// \brief Успешно, но точность меньше заданной.
//	///
//	Inaccurate = 1
//};

//#endif // RETURNCODE_H
