#ifndef ABSTRACTFUNCTION_H
#define ABSTRACTFUNCTION_H

#include <vector>
#include <string>

//#include <ReturnCode.h>

///
/// \brief Абстрактный класс для функций.
///
class AbstractFunction
{
public:
	AbstractFunction() = default;
	virtual ~AbstractFunction() = default;

	///
	/// \brief Имя функции.
	/// \return Возвращает имя функции
	///
	std::string getName()
	{
		return m_name;
	}

	//std::vector<double>* getIterations()
	//{
	//	return &m_iterarions;
	//}

	///
	/// \brief Возвращает строковое представление функции, подставляя букву как аргумент. Например "sin(x)".
	/// \return Строковое представление функции
	///
	virtual std::string ToString() = 0;
	///
	/// \brief Возвращает строковое представление функции, подставляя аргумент как число. Например "sin(0)".
	/// \return Строковое представление функции
	///
	virtual std::string ToStringArgs() = 0;
	///
	/// \brief Возвращает строковое представление функции, подставляя аргумент как число и результат. Например "sin(0) = 0".
	/// \return Строковое представление функции
	///
	virtual std::string ToStringArgsResult() = 0;

	///
	/// \brief Возвращает результат вычисления.
	/// \return Результат вычисления
	///
	double getResult()
	{
		return m_result;
	}

	///
	/// \brief Возвращает значение, определяющее, произведено ли вычисление.
	/// \return Значение, определяющее, произведено ли вычисление
	///
	bool isCalculated()
	{
		return m_isCalculated;
	}

	///
	/// \brief Возвращает точность вычисления.
	/// \return Точность вычисления
	///
	double getEps()
	{
		return m_eps;
	}

	///
	/// \brief Устанавливает точность вычисления.
	/// \param eps Точность вычисления
	///
	void setEps(double eps)
	{
		m_isCalculated = false;
		m_eps = eps;
	}

	///
	/// \brief Определяет, находится ли аргумент в зоне определения функции.
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	virtual bool isInDefinitionArea() = 0;

	///
	/// \brief Перечисление, представляющее статус вычисления.
	///
	enum class ReturnCode
	{
		///
		/// \brief Успешно.
		///
		Success = 0,
		///
		/// \brief Неудача, неизвестная причина.
		///
		Fail = -1,
		///
		/// \brief Неудача, значения аргумента не в зоне определения.
		///
		NotInDefinitionArea = -2,
		///
		/// \brief Успешно, но точность меньше заданной.
		///
		Inaccurate = 1
	};

	///
	/// \brief Производит вычисление.
	/// \return Статус вычисления
	///
	virtual ReturnCode Calculate() = 0;

protected:
	///
	/// \brief Точность вычисления.
	///
	double m_eps = 1e-8;
	///
	/// \brief Результат вычисления.
	///
	double m_result;

	///
	/// \brief Значение, определяющее, произведено ли вычисление.
	///
	bool m_isCalculated = false;

	//std::vector<double> m_iterarions;

	///
	/// \brief Название функции.
	///
	std::string m_name;
};

#endif // ABSTRACTFUNCTION_H
