#include "AbstractFunctionX.h"

std::string AbstractFunctionX::ToString()
{
	return getName() + "(x)";
}

std::string AbstractFunctionX::ToStringArgs()
{
	return getName() + "(" + std::to_string(getX()) + ")";
}

std::string AbstractFunctionX::ToStringArgsResult()
{
	return getName() + "(" + std::to_string(getX()) + ") = " + std::to_string(getResult());
}
