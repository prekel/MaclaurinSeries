#include "SumXY.h"

SumXY::SumXY()
{
	m_name = "sum";
}

bool SumXY::isInDefinitionArea()
{
	return true;
}

AbstractFunction::ReturnCode SumXY::Calculate()
{
	m_result = m_x + m_y;
	m_isCalculated = true;
	return ReturnCode::Success;
}
