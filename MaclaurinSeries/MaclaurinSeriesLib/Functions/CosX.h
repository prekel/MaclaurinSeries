#ifndef COSX_H
#define COSX_H

#include "AbstractFunctionX.h"

///
/// \brief Класс для вычисления косинуса cos(x).
///
class CosX : public AbstractFunctionX
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса CosX.
	///
	CosX();

	///
	/// \brief Проверяет, находится ли аргумент в области определения косинуса (всегда true).
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // COSX_H
