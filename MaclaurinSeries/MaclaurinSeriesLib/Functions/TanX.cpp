#include <cmath>
#include "TanX.h"

TanX::TanX()
{
	m_name = "tan";
}

bool TanX::isInDefinitionArea()
{
	auto pi = 3.14159265358979323846264338327950288419;
	return -pi / 2 < m_x && m_x < pi / 2;
}

double Bnom[] = {1, -1, 1, 0, -1, 0, 1, 0, -1, 0, 5, 0, -691, 0, 7, 0, -3617, 0, 43867, 0, -174611, 0, 854513, 0, -236364091, 0, 8553103, 0, -23749461029, 0, 8615841276005, 0, -7709321041217, 0, 2577687858367, 0, -26315271553053477373.0, 0, 2929993913841559, 0, -261082718496449122051.0};
double Bdenom[] = {1, 2, 6, 1, 30, 1, 42, 1, 30, 1, 66, 1, 2730, 1, 6, 1, 510, 1, 798, 1, 330, 1, 138, 1, 2730, 1, 6, 1, 870, 1, 14322, 1, 510, 1, 6, 1, 1919190, 1, 6, 1, 13530, 1, 1806, 1, 690, 1, 282, 1, 46410, 1, 66, 1, 1590, 1, 798, 1, 870, 1, 354, 1, 56786730, 1};

AbstractFunction::ReturnCode TanX::Calculate()
{
	if (!isInDefinitionArea()) return ReturnCode::NotInDefinitionArea;
	auto x = m_x;
	auto n = 1;
	auto sum = 0.0;
	auto nomx = x;
	auto denom = 2.0;
	auto i = 2;
	auto plus4powN = 4.0;
	auto minus4powN = -4.0;
	for (auto j = 0; j < 100; j++)
	{
		if ((j + 1) * 2 >= 40)
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Inaccurate;
		}
		auto a = ((Bnom[(j + 1) * 2] * minus4powN * (1 - plus4powN))  / (Bdenom[(j + 1) * 2] * denom)) * nomx;
		if (std::isinf(a) || std::isnan(a))
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Inaccurate;
		}
		sum += a;
		if (fabs(a) < m_eps)
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Success;
		}
		minus4powN = minus4powN *  -4.0;
		plus4powN = plus4powN *  4.0;
		nomx = nomx * x * x;
		denom = denom * (i + 1) * (i + 2);
		i += 2;
	}
	return ReturnCode::Fail;
}
