#ifndef SECX_H
#define SECX_H

#include "AbstractFunctionX.h"

///
/// \brief Класс для вычисления секанса sec(x).
///
class SecX : public AbstractFunctionX
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса SecX.
	///
	SecX();

	///
	/// \brief Проверяет, находится ли аргумент X в интервале (-pi/2; pi/2).
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // SECX_H
