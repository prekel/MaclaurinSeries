#ifndef SINX_H
#define SINX_H

#include "AbstractFunctionX.h"

///
/// \brief Класс для вычисления синуса sin(x).
///
class SinX : public AbstractFunctionX
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса SinX.
	///
	SinX();

	///
	/// \brief Проверяет, находится ли аргумент в области определения синуса (всегда true).
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // SINX_H
