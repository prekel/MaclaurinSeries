#include <cmath>
#include "PowOnePlusXY.h"

PowOnePlusXY::PowOnePlusXY()
{
	m_name = "pow1plus";
}

bool PowOnePlusXY::isInDefinitionArea()
{
	return -1 < m_x && m_x < 1;
}

AbstractFunction::ReturnCode PowOnePlusXY::Calculate()
{
	if (!isInDefinitionArea()) return ReturnCode::NotInDefinitionArea;
	auto x = m_x;
	auto y = m_y;
	auto sum = 1.0;
	auto nom = y * x;
	auto denom = 1.0;
	auto i = 0;
	auto n = 0;
	for (auto j = 0; j < 100; j++)
	{
		auto a = nom / denom;
		sum += a;
		if (fabs(a) < m_eps || n == 100)
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Success;
		}
		i++;
		nom = nom * (y - i) * x;
		denom = denom * (i + 1);
	}
	return ReturnCode::Fail;
}
