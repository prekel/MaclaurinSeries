#ifndef EXPX_H
#define EXPX_H

#include "AbstractFunctionX.h"

///
/// \brief Класс для вычисления экспоненциальной функции exp(x).
///
class ExpX : public AbstractFunctionX
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса ExpX.
	///
	ExpX();

	///
	/// \brief Проверяет, находится ли аргумент в области определения экспоненты (всегда true).
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // EXPX_H
