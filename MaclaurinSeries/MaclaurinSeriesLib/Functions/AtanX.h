#ifndef ATANX_H
#define ATANX_H

#include "AbstractFunctionX.h"

///
/// \brief Класс для вычисления арктангенса atan(x).
///
class AtanX : public AbstractFunctionX
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса AtanX.
	///
	AtanX();

	///
	/// \brief Проверяет, находится ли аргумент в отрезке [-1; 1].
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // ATANX_H
