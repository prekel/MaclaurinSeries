#ifndef SUMXY_H
#define SUMXY_H

#include "AbstractFunctionXY.h"

///
/// \brief Класс для вычисления суммы sum(x, y) a.k.a. x + y.
///
class SumXY : public AbstractFunctionXY
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса SumXY.
	///
	SumXY();

	///
	/// \brief Проверяет, находится ли аргументы в области определения суммы (всегда true).
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // SUMXY_H
