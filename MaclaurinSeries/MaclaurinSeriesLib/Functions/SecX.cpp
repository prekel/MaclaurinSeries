#include <cmath>
#include "SecX.h"

SecX::SecX()
{
	m_name = "sec";
}

bool SecX::isInDefinitionArea()
{
	auto pi = 3.14159265358979323846264338327950288419;
	return -pi / 2 < m_x && m_x < pi / 2;
}

double Enumb[] = {1, -1, 5, -61, 1385, -50521, 2702765, -199360981, 19391512145, -2404879675441, 370371188237525, -69348874393137901.0, 15514534163557086905.0, -4087072509293123892361.0, 1252259641403629865468285.0, -441543893249023104553682821.0, 177519391579539289436664789665.0};

AbstractFunction::ReturnCode SecX::Calculate()
{
	if (!isInDefinitionArea()) return ReturnCode::NotInDefinitionArea;
	auto x = m_x;
	auto sum = 0.0;
	auto nomx = 1.0;
	auto denom = 1.0;
	auto i = 0.0;
	auto minus1 = 1.0;
	for (auto j = 0; j < 100; j++)
	{
		if (j > 16)
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Inaccurate;
		}
		auto a = (minus1 * Enumb[j]) / denom * nomx;
		if (std::isinf(a) || std::isnan(a))
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Inaccurate;
		}
		sum += a;
		if (fabs(a) < m_eps)
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Success;
		}
		minus1 *= -1;
		nomx = nomx * x * x;
		denom = denom * (i + 1) * (i + 2);
		i += 2;
	}
	return ReturnCode::Fail;
}
