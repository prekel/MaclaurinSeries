#ifndef ALL_H
#define ALL_H

#include <AbstractFunction.h>
#include <AbstractFunctionX.h>
#include <AbstractFunctionXY.h>

#include <Functions/ExpX.h>
#include <Functions/SinX.h>
#include <Functions/CosX.h>
#include <Functions/LnOnePlusX.h>
#include <Functions/PowOnePlusXY.h>
#include <Functions/AtanX.h>
#include <Functions/SumXY.h>
#include <Functions/TanX.h>
#include <Functions/SecX.h>

#endif // ALL_H
