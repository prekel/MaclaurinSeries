#ifndef LNONEPLUSX_H
#define LNONEPLUSX_H

#include "AbstractFunctionX.h"

///
/// \brief Класс для вычисления натурального логарифма ln(1 + x).
///
class LnOnePlusX : public AbstractFunctionX
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса LnOnePlusX.
	///
	LnOnePlusX();

	///
	/// \brief Проверяет, находится ли аргумент в полуинтервале (-1; 1].
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // LNONEPLUSX_H
