#include <cmath>

#include "CosX.h"

CosX::CosX()
{
	m_name = "cos";
}

bool CosX::isInDefinitionArea()
{
	return true;
}

AbstractFunction::ReturnCode CosX::Calculate()
{
	if (!isInDefinitionArea()) return ReturnCode::NotInDefinitionArea;
	auto pi = 3.14159265358979323846264338327950288419;
	auto x = m_x - floor(fabs(m_x) / (2 * pi)) * 2 * pi * (m_x > 0 ? 1 : -1);
	auto n = 1;
	auto sum = 0.0;
	auto nom = 1.0;
	auto denom = 1.0;
	auto i = 0;
	for (auto j = 0; j < 100; j++)
	{
		auto a = nom / denom;
		sum += (n++ % 2 ? 1 : -1) * a;
		if (fabs(a) < m_eps)
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Success;
		}
		nom = nom * x * x;
		denom = denom * (i + 1) * (i + 2);
		i += 2;
	}
	return ReturnCode::Fail;
}
