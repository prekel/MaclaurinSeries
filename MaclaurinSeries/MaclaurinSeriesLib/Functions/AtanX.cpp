#include <cmath>
#include "AtanX.h"

AtanX::AtanX()
{
	m_name = "atan";
}

bool AtanX::isInDefinitionArea()
{
	return -1 <= m_x && m_x <= 1;
}

AbstractFunction::ReturnCode AtanX::Calculate()
{
	if (!isInDefinitionArea()) return ReturnCode::NotInDefinitionArea;
	auto x = m_x;
	auto n = 1;
	auto sum = 0.0;
	auto nom = x;
	auto denom = 1.0;
	for (auto j = 0; j < 2000; j++)
	{
		auto a = nom / denom;
		sum += (n++ % 2 ? 1 : -1) * a;
		if (fabs(a) < m_eps)
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Success;
		}
		nom = nom * x * x;
		denom += 2;
	}
	return ReturnCode::Fail;
}
