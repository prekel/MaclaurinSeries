#include <cmath>
#include "LnOnePlusX.h"

LnOnePlusX::LnOnePlusX()
{
	m_name = "ln1plus";
}

bool LnOnePlusX::isInDefinitionArea()
{
	return -1 < m_x && m_x <= 1;
}

AbstractFunction::ReturnCode LnOnePlusX::Calculate()
{
	if (!isInDefinitionArea()) return ReturnCode::NotInDefinitionArea;
	auto x = m_x;
	auto n = 1;
	auto sum = 0.0;
	auto nom = x;
	auto denom = 1.0;
	for (auto j = 0; j < 100; j++)
	{
		auto a = nom / denom;
		sum += (n++ % 2 ? 1 : -1) * a;
		if (fabs(a) < m_eps || n == 100)
		{
			m_isCalculated = true;
			m_result = sum;
			return ReturnCode::Success;
		}
		nom = nom * x;
		denom++;
	}
	return ReturnCode::Fail;
}
