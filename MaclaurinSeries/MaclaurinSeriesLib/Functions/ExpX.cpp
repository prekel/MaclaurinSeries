#include <cmath>
#include "ExpX.h"

ExpX::ExpX()
{
	m_name = "exp";
}

bool ExpX::isInDefinitionArea()
{
	return true;
}

AbstractFunction::ReturnCode ExpX::Calculate()
{
	if (!isInDefinitionArea()) return ReturnCode::NotInDefinitionArea;
	auto res = 0.0;
	auto up = 1.0;
	auto down = 1.0;
	//auto n = 1;
	auto i = 1.0;
	for (auto j = 0; j < 1000; j++)
	{
		auto a = up / down;
		if (std::isinf(a))
		{
			m_result = res;
			m_isCalculated = true;
			return ReturnCode::Inaccurate;
		}
		res += a;
		if ((fabs(a) < m_eps && i > 5))
		{
			m_result = res;
			m_isCalculated = true;
			return ReturnCode::Success;
		}
		up *= m_x;
		down *= i++;
	}
	return ReturnCode::Fail;
}
