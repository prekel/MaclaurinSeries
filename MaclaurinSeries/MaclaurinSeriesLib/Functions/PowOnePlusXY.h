#ifndef POWONEPLUSXY_H
#define POWONEPLUSXY_H

#include "AbstractFunctionXY.h"

///
/// \brief Класс для вычисления показательно-степенной функции pow(1 + x, y) a.k.a. (1 + x)^y.
///
class PowOnePlusXY : public AbstractFunctionXY
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса LnOnePlusX.
	///
	PowOnePlusXY();

	///
	/// \brief Проверяет, находится ли аргумент X в интервале (-1; 1).
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // POWONEPLUSXY_H
