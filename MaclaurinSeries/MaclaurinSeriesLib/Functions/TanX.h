#ifndef TANX_H
#define TANX_H

#include "AbstractFunctionX.h"

///
/// \brief Класс для вычисления тангенса tan(x).
///
class TanX : public AbstractFunctionX
{
public:
	///
	/// \brief Инициализирует новый экземпляр класса TanX.
	///
	TanX();

	///
	/// \brief Проверяет, находится ли аргумент в интервале (-pi/2; pi/2).
	/// \return Значение, определяющее, находится ли аргумент в зоне определения функции
	///
	bool isInDefinitionArea() override;
	ReturnCode Calculate() override;
};

#endif // TANX_H
