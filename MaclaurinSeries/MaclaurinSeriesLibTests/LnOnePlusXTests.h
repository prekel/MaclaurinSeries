#ifndef LNONEPLUSXTESTS_H
#define LNONEPLUSXTESTS_H

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/LnOnePlusX.h>

using namespace testing;

TEST(LnOnePlusXTests, LnOnePlusXTest1)
{
	LnOnePlusX f;

	f.setX(exp(0.1) - 1);
	f.setEps(1e-8);
	f.Calculate();

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	auto res = f.getResult();

	EXPECT_LT((0.1 - res), 1e-8);
}

#endif // LNONEPLUSXTESTS_H
