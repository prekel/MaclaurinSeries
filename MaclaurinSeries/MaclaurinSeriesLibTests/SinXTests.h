#pragma once

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/SinX.h>

using namespace testing;

TEST(SinXTests, SinXTest1)
{
	SinX f;

	f.setX(0);
	f.setEps(1e-8);
	f.Calculate();

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	auto res = f.getResult();

	EXPECT_LT((0 - res), 1e-8);
}

TEST(SinXTests, SinXTest2)
{
	SinX f;

	f.setEps(1e-8);

	std::vector<double> values = {-1, -2, -3, -10, -3.14, 0, 3.14, 100, -100};

	for (auto i : values)
	{
		f.setX(i);

		auto b = f.isInDefinitionArea();

		EXPECT_TRUE(b);

		f.Calculate();

		auto actual = f.getResult();
		auto expected = sin(i);

		EXPECT_LT((expected - actual), 1e-8);
	}
}
