#pragma once

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/TanX.h>

using namespace testing;

TEST(TanXTests, TanXTest1)
{
	TanX f;

	f.setX(0);
	f.setEps(1e-8);
	f.Calculate();

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	auto res = f.getResult();

	EXPECT_LT((0 - res), 1e-8);
}

TEST(TanXTests, TanXTest2)
{
	TanX f;

	f.setEps(1e-6);

	//std::vector<double> values = {-1, -2, -3, -10, -3.14, 0, 3.14, 100, -100};
	auto pi = 3.14159265358979323846264338327950288419;
	std::vector<double> values = {-1, 1, pi/3, -pi/3, 2 * pi / 7, -2 * pi / 5};

	for (auto i : values)
	{
		f.setX(i);

		auto b = f.isInDefinitionArea();

		EXPECT_TRUE(b);

		auto ret = f.Calculate();

		auto actual = f.getResult();
		auto expected = tan(i);

		EXPECT_LT((expected - actual), 1e-6);
	}
}
