#ifndef SUMXYTESTS_H
#define SUMXYTESTS_H

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/SumXY.h>

using namespace testing;

TEST(SumXYTests, SumXYTest1)
{
	SumXY f;

	f.setX(0.5);
	f.setY(3);
	f.setEps(1e-8);

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	f.Calculate();

	auto res = f.getResult();

	EXPECT_LT((3.5 - res), 1e-8);
}

#endif // SUMXYTESTS_H
