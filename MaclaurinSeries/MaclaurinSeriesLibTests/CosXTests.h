#ifndef COSXTESTS_H
#define COSXTESTS_H

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/CosX.h>

using namespace testing;

TEST(CosXTests, CosXTest1)
{
	CosX f;

	f.setX(0);
	f.setEps(1e-8);
	f.Calculate();

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	auto res = f.getResult();

	EXPECT_LT((1 - res), 1e-8);
}

TEST(CosXTests, CosXTest2)
{
	CosX f;

	f.setEps(1e-8);

	std::vector<double> values = {-1, -2, -3, -10, -3.14, 0, 3.14, 100, -100};

	for (auto i : values)
	{
		f.setX(i);

		auto b = f.isInDefinitionArea();

		EXPECT_TRUE(b);

		f.Calculate();

		auto actual = f.getResult();
		auto expected = cos(i);

		EXPECT_LT((expected - actual), 1e-8);
	}
}

#endif // COSXTESTS_H
