#pragma once

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/SecX.h>

using namespace testing;

TEST(SecXTests, SecXTest1)
{
	SecX f;

	auto pi = 3.14159265358979323846264338327950288419;
	f.setX(pi / 3);
	f.setEps(1e-5);
	auto ret = f.Calculate();

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	auto res = f.getResult();

	EXPECT_LT((2 - res), 1e-5);
}

//TEST(SecXTests, SecXTest2)
//{
//	SecX f;

//	f.setEps(1e-8);

//	std::vector<double> values = {-1, -2, -3, -10, -3.14, 0, 3.14, 100, -100};

//	for (auto i : values)
//	{
//		f.setX(i);

//		auto b = f.isInDefinitionArea();

//		EXPECT_TRUE(b);

//		f.Calculate();

//		auto actual = f.getResult();
//		auto expected = (i);

//		EXPECT_LT((expected - actual), 1e-8);
//	}
//}
