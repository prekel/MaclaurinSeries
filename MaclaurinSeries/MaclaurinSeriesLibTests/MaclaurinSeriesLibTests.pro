include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
CONFIG -= qt

HEADERS += \
    SinXTests.h \
    AbstractFunctionTests.h \
    AbstractFunctionXTests.h \
    ExpXTests.h \
    CosXTests.h \
    LnOnePlusXTests.h \
    PowOnePlusXYTests.h \
    AtanXTests.h \
    SumXYTests.h \
    TanXTests.h \
    SecXTests.h


SOURCES += \
        main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../MaclaurinSeriesLib/release/ -lMaclaurinSeriesLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../MaclaurinSeriesLib/debug/ -lMaclaurinSeriesLib
else:unix: LIBS += -L$$OUT_PWD/../MaclaurinSeriesLib/ -lMaclaurinSeriesLib

INCLUDEPATH += $$PWD/../MaclaurinSeriesLib
DEPENDPATH += $$PWD/../MaclaurinSeriesLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/release/libMaclaurinSeriesLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/debug/libMaclaurinSeriesLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/release/MaclaurinSeriesLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/debug/MaclaurinSeriesLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/libMaclaurinSeriesLib.a
