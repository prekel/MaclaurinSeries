#ifndef POWONEPLUSXYTESTS_H
#define POWONEPLUSXYTESTS_H

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/PowOnePlusXY.h>

using namespace testing;

TEST(PowOnePlusXYTests, PowOnePlusXYTest1)
{
	PowOnePlusXY f;

	f.setX(0.5);
	f.setY(3);
	f.setEps(1e-8);

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	f.Calculate();

	auto res = f.getResult();

	EXPECT_LT((3.375 - res), 1e-8);
}

#endif // POWONEPLUSXYTESTS_H
