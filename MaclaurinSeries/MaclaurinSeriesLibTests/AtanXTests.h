#ifndef ATANXTESTS_H
#define ATANXTESTS_H

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/AtanX.h>

using namespace testing;

TEST(AtanXTests, AtanXTest1)
{
	AtanX f;

	f.setX(0.5);
	f.setEps(1e-8);
	f.Calculate();

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	auto res = f.getResult();

	EXPECT_LT((atan(0.5) - res), 1e-8);
}

#endif // ATANXTESTS_H
