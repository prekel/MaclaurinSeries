#ifndef EXPXTESTS_H
#define EXPXTESTS_H

#include <vector>

#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>

#include <Functions/ExpX.h>

using namespace testing;

TEST(ExpXTests, ExpXTest1)
{
	ExpX f;

	f.setX(0);
	f.setEps(1e-8);
	f.Calculate();

	auto b = f.isInDefinitionArea();

	EXPECT_TRUE(b);

	auto res = f.getResult();

	EXPECT_LT((1 - res), 1e-8);
}

TEST(ExpXTests, ExpXTest2)
{
	ExpX f;

	f.setEps(1e-8);

	std::vector<double> values = {10, 5, -2, -3, -10, -3.14, 0, 3.14};

	for (auto i : values)
	{
		f.setX(i);

		auto b = f.isInDefinitionArea();

		EXPECT_TRUE(b);

		f.Calculate();

		auto actual = f.getResult();
		auto expected = exp(i);

		EXPECT_LT((expected - actual), 1e-8);
	}
}

TEST(ExpXTests, ExpXTest3)
{
	ExpX f;

	f.setEps(1e-5);

	std::vector<double> values = {100, 55, 150, 50, 10};

	for (auto i : values)
	{
		f.setX(i);

		auto b = f.isInDefinitionArea();

		//if (i >= 50) EXPECT_FALSE(b);
		//if (!b) continue;
		EXPECT_TRUE(b);

		f.Calculate();

		auto actual = f.getResult();
		auto expected = exp(i);
		auto log10actual = log10(actual);
		auto log10expected = log10(expected);

		EXPECT_LT((log10expected - log10actual), 1);
		//EXPECT_LT((expected - actual), 1e+10);
	}
}

TEST(ExpXTests, ExpXMaxTest1)
{
	ExpX f;

	f.setEps(1e-5);

	std::vector<double> values;
	std::vector<AbstractFunction::ReturnCode> results;
	std::vector<double> trueresults;
	for (auto i = 0; i < 1000; i++)
	{
		values.push_back(i);
		f.setX(i);
		results.push_back(f.Calculate());
		trueresults.push_back(exp(i));

		//EXPECT_EQ(results.back(), trueresults.back());
	}
}


#endif // EXPXTESTS_H
