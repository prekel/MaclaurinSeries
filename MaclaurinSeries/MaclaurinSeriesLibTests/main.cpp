#include <gtest/gtest.h>

#include "ExpXTests.h"
#include "SinXTests.h"
#include "CosXTests.h"
#include "LnOnePlusXTests.h"
#include "PowOnePlusXYTests.h"
#include "AtanXTests.h"
#include "SumXYTests.h"
#include "TanXTests.h"
#include "SecXTests.h"

int main(int argc, char *argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
