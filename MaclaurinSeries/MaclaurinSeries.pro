TEMPLATE = subdirs

SUBDIRS += \
    MaclaurinSeriesLib \
    MaclaurinSeriesLibTests \
    MaclaurinSeriesGuiLib \
    MaclaurinSeriesGui

MaclaurinSeriesLibTests.depends = MaclaurinSeriesLib
MaclaurinSeriesGuiLib.depends = MaclaurinSeriesLib
MaclaurinSeriesGui.depends = MaclaurinSeriesLib MaclaurinSeriesGuiLib
