QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MaclaurinSeriesGuiLib
TEMPLATE = lib
CONFIG += staticlib

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += \
        MainWindow.cpp

HEADERS += \
        MainWindow.h

FORMS += \
        MainWindow.ui

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../MaclaurinSeriesLib/release/ -lMaclaurinSeriesLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../MaclaurinSeriesLib/debug/ -lMaclaurinSeriesLib
else:unix: LIBS += -L$$OUT_PWD/../MaclaurinSeriesLib/ -lMaclaurinSeriesLib

INCLUDEPATH += $$PWD/../MaclaurinSeriesLib
DEPENDPATH += $$PWD/../MaclaurinSeriesLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/release/libMaclaurinSeriesLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/debug/libMaclaurinSeriesLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/release/MaclaurinSeriesLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/debug/MaclaurinSeriesLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../MaclaurinSeriesLib/libMaclaurinSeriesLib.a
