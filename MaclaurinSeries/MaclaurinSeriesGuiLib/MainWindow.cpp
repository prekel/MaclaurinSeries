#include <QtMath>

#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	m_functions = new QVector<AbstractFunction*>();
	m_functions->append(new ExpX());
	m_functions->append(new SinX());
	m_functions->append(new CosX());
	m_functions->append(new LnOnePlusX());
	m_functions->append(new PowOnePlusXY());
	m_functions->append(new AtanX());
	m_functions->append(new SumXY());
	m_functions->append(new TanX());
	m_functions->append(new SecX());

	for (auto i = 0; i < m_functions->size(); i++)
	{
		ui->comboBox_function->addItem(QString::fromStdString(m_functions->at(i)->ToString()));
	}
}

MainWindow::~MainWindow()
{
	for (auto i : *m_functions)
	{
		delete i;
	}
	delete m_functions;
	delete ui;
}

void MainWindow::Calculate()
{
	auto ok = true;
	auto okall = true;
	auto x = ui->lineEdit_x->text().toDouble(&ok);
	okall &= ok;
	auto eps = ui->lineEdit_eps->text().toDouble(&ok);
	okall &= ok;

	if (!okall)
	{
		ui->statusBar->showMessage("Wrong format");
		return;
	}

	auto ind = ui->comboBox_function->currentIndex();
	auto f = m_functions->at(ind);

	f->setEps(eps);

	if (ind != 4 && ind != 6)
	{
		auto fx = static_cast<AbstractFunctionX*>(f);
		fx->setX(x);
	}
	else
	{
		auto y = ui->lineEdit_y->text().toDouble(&ok);
		if (!ok)
		{
			ui->statusBar->showMessage("Wrong format");
			return;
		}
		auto fxy = static_cast<AbstractFunctionXY*>(f);
		fxy->setX(x);
		fxy->setY(y);
	}

	auto ret = f->Calculate();

	if (static_cast<int>(ret) >= 0)
	{
		auto result = f->getResult();
		auto precision = int(ceil(-log10(eps)));
		ui->lineEdit_result->setText(QString::number(result, 'f', precision));
	}
	else
	{
		ui->lineEdit_result->setText("");
	}

	if (ret == AbstractFunction::ReturnCode::Success)
	{
		ui->statusBar->showMessage("Calculated: " + QString::fromStdString(f->ToStringArgsResult()));
	}
	if (ret == AbstractFunction::ReturnCode::Inaccurate)
	{
		ui->statusBar->showMessage("Calculated, but not accurate: " + QString::fromStdString(f->ToStringArgsResult()));
	}
	if (ret == AbstractFunction::ReturnCode::Fail)
	{
		ui->statusBar->showMessage("Unknown error");
	}
	if (ret == AbstractFunction::ReturnCode::NotInDefinitionArea)
	{
		ui->statusBar->showMessage("Not in definition area");
	}
}

void MainWindow::Reset()
{
	ui->lineEdit_x->clear();
	ui->lineEdit_y->clear();
	ui->lineEdit_eps->setText("1e-8");
	ui->lineEdit_result->clear();
	ui->comboBox_function->setCurrentIndex(0);
}


void MainWindow::on_pushButton_calculate_clicked()
{
	Calculate();
}

void MainWindow::on_pushButton_reset_clicked()
{
	Reset();
}

void MainWindow::on_comboBox_function_currentIndexChanged(int index)
{
	if (index == 4 || index == 6)
	{
		ui->lineEdit_y->setEnabled(true);
	}
	else
	{
		ui->lineEdit_y->setEnabled(false);
	}
}
