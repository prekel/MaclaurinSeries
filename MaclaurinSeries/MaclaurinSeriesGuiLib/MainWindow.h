#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>

#include <Functions/All.h>


namespace Ui {
class MainWindow;
}

///
/// \brief Класс главного окна
///
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	///
	/// \brief Иницилиалицирует новый экземпляр класса MainWindow
	/// \param parent Родительский объект
	///
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private slots:
	///
	/// \brief Вызывается при нажании на кнопку Calculate
	///
	void on_pushButton_calculate_clicked();

	///
	/// \brief Вызывается при нажании на кнопку Reset
	///
	void on_pushButton_reset_clicked();

	///
	/// \brief Вызывается при изменения выбора в comboBox
	/// \param index Индекс выбранного элемента
	///
	void on_comboBox_function_currentIndexChanged(int index);

private:
	Ui::MainWindow *ui;

	///
	/// \brief Массив функций
	///
	QVector<AbstractFunction*> *m_functions;

	///
	/// \brief Выполнить вычисление
	///
	void Calculate();
	///
	/// \brief Сбросить
	///
	void Reset();
};

#endif // MAINWINDOW_H
